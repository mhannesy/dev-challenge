package controllers;

import java.util.Optional;

import javax.inject.Inject;

import models.Movie;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.MovieJdbcService;

public class MovieController extends Controller {

	@Inject MovieJdbcService service;
	
	private Movie composeResponse(Long id, Optional<String> from, Optional<String> to) {
		Movie movie = service.fetchMovie(id);
		if (movie != null) {
			movie.setRatings(service.fetchRatings(id, from, to));
		}
		return movie;
	}
	
	public Result ratings(Long id, Optional<String> from, Optional<String> to) {
		
		Movie response = composeResponse(id, from, to);
		if (response == null) {
			return notFound("No movie found with id " + id);
		} else {
	       return ok(Json.toJson(response));			
		}
    }
}