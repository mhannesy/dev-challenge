import play.http.HttpErrorHandler;
import play.mvc.*;
import play.mvc.Http.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import javax.inject.Singleton;

@Singleton
public class ErrorHandler implements HttpErrorHandler {
	public CompletionStage<Result> onClientError(RequestHeader request, int statusCode, String message) {
		switch (statusCode) {
			case Http.Status.NOT_FOUND:
				message = "Resource not found.";
				break;
			default:
				message = "Request not valid.";
				break;
		}
		return CompletableFuture.completedFuture(Results.status(statusCode, message)
        );
    }

    public CompletionStage<Result> onServerError(RequestHeader request, Throwable exception) {
        return CompletableFuture.completedFuture(
                Results.internalServerError("A server error occurred.")
        );
    }
}