package models;

import java.util.Date;

public class Rating {
	
	private float rating;
	private Date received;
	
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public Date getDate() {
		return received;
	}
	public void setDate(Date date) {
		this.received = date;
	}
}
