package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import models.Movie;
import models.Rating;
import play.Logger;
import play.db.Database;

public class MovieJdbcService {

	private Database database;
	
	private static final String REGEX_GENRE_SEPARATOR = "\\|";

	@Inject
	public MovieJdbcService(Database database) {
		this.database = database;
	}

	protected List<String> convertGenres(String genreColumn) {
		return Arrays.stream(genreColumn.split(REGEX_GENRE_SEPARATOR))
				.filter(genre -> !genre.isEmpty())
				.map(genre -> genre.trim())
				.collect(Collectors.toList());
	}
	
	/**
	 * Convert result set to POJO. 
	 * @param resultSet
	 * @return Movie object
	 */
	private Movie convertMovie(ResultSet resultSet) {
		
		Movie result = null;
		
		try {
			if (resultSet != null && resultSet.next()) {
				result = new Movie();
				result.setTitle(resultSet.getString(1));
				result.setGenres(convertGenres(resultSet.getString(2)));
			}
		} catch (SQLException sqle) {
			Logger.error("Problems trying to convert movie data", sqle);
		}
		
		return result;
	}
	
	/**
	 * Convert result set to POJOs.
	 * @param resultSet
	 * @return List of Ratings objects
	 */
	private List<Rating> convertRatings(ResultSet resultSet) {
		
		List<Rating> result = new ArrayList<Rating>();
		
		try {
			while (resultSet != null && resultSet.next()) {
				Rating rating = new Rating();
				rating.setRating(resultSet.getLong(1));
				rating.setDate(resultSet.getDate(2));
				result.add(rating);
			}
			
		} catch (SQLException sqle) {
			Logger.error("Problems trying to convert movie data", sqle);
		}
		
		return result;
	}
	
	
	/**
	 * Query database for movie information.
	 * 
	 * @param id
	 * @return found Movie or null
	 */
	public Movie fetchMovie(Long id) {
		
		
		Movie result = null;
		
		String query = "select m.title, m.genres from movies as m where m.id=?";
		
		try {
			Connection connection = database.getConnection();
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setLong(1, id);
			Logger.debug("Movie statement: " + statement);
			ResultSet resultSet = statement.executeQuery();
			result = convertMovie(resultSet);
			connection.close();
		} catch (SQLException sqle) {
			Logger.error("Problems trying to fetch movie data", sqle);
		}		
		return result; 
	}
	
	/**
	 * Query database for ratings for given movie. 
	 * 
	 * @param movieId
	 * @param from
	 * @param to
	 * @return list of Ratings
	 */
	public List<Rating> fetchRatings(Long movieId, Optional<String> from, Optional<String> to) {
		
		List<Rating> result = new ArrayList<Rating>();
		
		String query = "select r.rating, r.received "
				+ "from ratings as r where r.movieid = ? "
				+ getDateFilter(from, to)
				+ "order by r.received";
		
		try {
			Connection connection = database.getConnection();
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setLong(1, movieId);
			Logger.debug("Ratings statement: " + statement);
			ResultSet resultSet = statement.executeQuery();
			result = convertRatings(resultSet);
			connection.close();
		} catch (SQLException sqle) {
			Logger.error("Problems trying to fetch ratings data", sqle);
		}

		return result;
	}
	
	/**
	 * Validate given from/to date input string.
	 * 
	 * @param input
	 * @return true if valid date
	 */
	private boolean isValidISO8601(String input) {
		try {
			LocalDate.parse(input);
			return true;
		} catch (DateTimeParseException dtpe) {
			Logger.warn("Input " + input + " is not valid date for filtering", dtpe);
			return false;
		}
	}
	
	
	/**
	 * Generate date filter from given parameters.
	 * 
	 * @param from
	 * @param to
	 * @return predicates, for example " and r.received >= '2001-01-01' and r.received <= '2001-03-03'" or empty string
	 * 
	 */
	protected String getDateFilter(Optional<String> from, Optional<String> to) {
		return from.filter(f -> isValidISO8601(f))
				.map(valid -> "and r.received >= '" + valid + "' ")
				.orElse("")
				+ to.filter(t -> isValidISO8601(t))
				.map(valid -> "and r.received <= '" + valid + "' ")
				.orElse("");				
	}
}
