# Dev Challenge solution

## Requirements
- sbt 1.1.1
- Docker 17.12.0-ce
- 8u152-zulu JDK

## Set up database and import data

Download Movielens data:

`curl --create-dirs -o data/ml-latest.zip http://files.grouplens.org/datasets/movielens/ml-latest.zip`

`unzip -d data data/ml-latest.zip`

Run PostgreSQL server and client and log into PostgreSQL client with password `AgwYvaOJ09GII`.

`docker run --name movielens-postgres -e POSTGRES_PASSWORD=AgwYvaOJ09GII -d -p 5432:5432 postgres`

`docker run --name postgres-client -it --rm --link movielens-postgres:postgres postgres psql -h postgres -U postgres`

Copy data to client container:

`docker cp data/ml-latest/movies.csv postgres-client:/tmp/movies.csv`

`docker cp data/ml-latest/ratings.csv postgres-client:/tmp/ratings.csv`

Create database schema with PostgreSQL client:

```
create database movielens;

\c movielens

create table movies (
    id integer primary key,
    title varchar(200),
    genres varchar(200));
create table ratings (
    userId integer,
    movieId integer references movies(id),
    rating numeric(2,1) not null,
    timestamp integer not null,
    id serial primary key);
```

Import data and migrate timestamp to date type column:
```
\copy movies(id, title, genres) FROM '/tmp/movies.csv' HEADER DELIMITER ',' CSV
\copy ratings(userId, movieId, rating, timestamp) FROM '/tmp/ratings.csv' HEADER DELIMITER ',' CSV

alter table ratings add column received date;
update ratings set received = to_timestamp(timestamp);
alter table ratings drop column timestamp;

create user javaclient;
alter user javaclient with encrypted password 'zQO1IzChcZEdg';
grant select on movies, ratings to javaclient;
```

## Run application

Run in test mode with SBT:

`sbt run`

Run in production mode:

`sbt clean stage runProd`

Create request to API: http://localhost:9000/movie/356?from=2001-01-01&to=2001-03-03