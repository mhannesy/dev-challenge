name := """dev-challenge"""
organization := "org.grouplens.movies"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.4"

libraryDependencies += guice

libraryDependencies += javaJdbc
libraryDependencies += "org.postgresql" % "postgresql" % "42.2.1"
libraryDependencies ++= Seq(
  ehcache
)
libraryDependencies += "org.mockito" % "mockito-core" % "2.15.0" % "test"