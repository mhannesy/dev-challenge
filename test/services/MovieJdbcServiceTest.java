package services;

import org.junit.Before;
import org.junit.Test;

import play.db.Database;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Optional;

public class MovieJdbcServiceTest {
	
	private MovieJdbcService service;
	
	@Before
	public void setUp() {
		service = new MovieJdbcService(mock(Database.class));
	}

	@Test
	public void testConvertGenres() {
		// Verify that only non-empty, trimmed strings are included
		assertEquals(Arrays.asList(new String[] { "abc", "def", "123", "456" }),
				service.convertGenres("|abc |def|123||456"));
	}
	
	@Test
	public void testGetDateFilter() {
		// Verify no filter created for empty parameters
		assertEquals("", service.getDateFilter(Optional.empty(), Optional.empty()));
		// Verify from date filter
		assertEquals("and r.received >= '2001-12-31' ",
				service.getDateFilter(Optional.of("2001-12-31"), Optional.empty()));
		// Verify to date filter
		assertEquals("and r.received <= '2001-12-31' ",
				service.getDateFilter(Optional.empty(), Optional.of("2001-12-31")));
	}
}
